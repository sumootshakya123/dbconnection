﻿namespace MySecondApp.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string ISBN { get; set; }
        public string Author { get; set; }
    }
}
