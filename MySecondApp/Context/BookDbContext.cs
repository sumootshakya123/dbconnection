﻿using Microsoft.EntityFrameworkCore;

namespace MySecondApp.Models
{
    public class BookDbContext : DbContext
    {
            public BookDbContext(DbContextOptions<BookDbContext> options)
                : base(options) { }
            public DbSet<Book> Book { get; set; }
    }
}
