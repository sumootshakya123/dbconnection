﻿using MySecondApp.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MySecondApp.Repos
{
    public interface UserRepo
    {
        Task<IEnumerable<User>> GetUsers();
        Task AddUser(User user);
        Task<User> UpdateUser(User user);
    }
}
