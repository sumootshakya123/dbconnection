﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MySecondApp.Models;

namespace MySecondApp.Repos
{
    public interface BookRepo
    {
        Task<IEnumerable<Book>> GetBooks();
        Task AddBook(Book book);
        Task<Book> UpdateBook(Book book);
    }
}
