﻿using Microsoft.EntityFrameworkCore;
using MySecondApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MySecondApp.Repos.Impl
{
    public class UserRepoImpl : UserRepo
    {
        private readonly UserDbContext _context;
        public UserRepoImpl(UserDbContext context)
        {
            _context = context;
        }
        public async Task AddUser(User user)
        {
            await _context.User.AddAsync(user);
            await _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _context.User.ToListAsync();
        }

        public async Task<User> UpdateUser(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            _context.SaveChanges();
            return user;
        }
    }
}
