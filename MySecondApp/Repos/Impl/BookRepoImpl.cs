﻿using Microsoft.EntityFrameworkCore;
using MySecondApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MySecondApp.Repos.Impl
{
    public class BookRepoImpl : BookRepo
    {
        private readonly BookDbContext _context;
        public BookRepoImpl(BookDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Book>> GetBooks()
        {
            var books = await _context.Book.ToListAsync();
            return books;
        }
        public async Task AddBook(Book book)
        {
            await _context.Book.AddAsync(book);
            await _context.SaveChangesAsync();
        }
        public async Task<Book> UpdateBook(Book book)
        {
            _context.Entry(book).State = EntityState.Modified;
            _context.SaveChanges();
            return book;
        }
    }
}
