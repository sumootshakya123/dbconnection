﻿using Microsoft.AspNetCore.Mvc;
using MySecondApp.Models;
using MySecondApp.Repos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MySecondApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly BookRepo _bookRepository;
        public BookController(BookRepo bookRepository)
        {
            _bookRepository = bookRepository;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Book>>> GetAllUsers()
        {
            var books = await _bookRepository.GetBooks();
            return Ok(books);
        }
        [HttpPost]
        public async Task<ActionResult<Book>> Post([FromBody] Book book)
        {
            await _bookRepository.AddBook(book);
            return Ok();
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<Book>> Update([FromRoute] int Id, [FromBody] Book book)
        {
            if (Id != book.Id)
                return BadRequest();

            var updatedBook = await _bookRepository.UpdateBook(book);
            if (updatedBook != null)
                return Ok(updatedBook);

            return NotFound();

        }

    }
}
