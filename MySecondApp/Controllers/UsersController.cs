﻿using Microsoft.AspNetCore.Mvc;
using MySecondApp.Models;
using MySecondApp.Repos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MySecondApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserRepo _userRepository;
        public UsersController(UserRepo userRepository)
        {
            _userRepository = userRepository;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetAllUsers()
        {
            var users = await _userRepository.GetUsers();
            return Ok(users);
        }
        [HttpPost]
        public async Task<ActionResult<User>> Post([FromBody] User user)
        {
            await _userRepository.AddUser(user);
            return Ok();
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<User>> Update([FromRoute] int Id, [FromBody] User user)
        {
            if (Id != user.Id)
                return BadRequest();
            
            var updatedUser = await _userRepository.UpdateUser(user);
            if (updatedUser != null)
                return Ok(updatedUser);

            return NotFound();

        }
    }
}